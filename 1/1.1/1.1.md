
## 2020.3.24

* 1.PicGO测试 
![](https://gitlab.com/pic-02/pic-ma02/uploads/3e5705f06f9caf0bb262811d3f8ea98e/WechatIMG445.png)

* 2.关联文件夹

输入cd 拖入文件夹
  
  出现问题
 ![](https://gitlab.com/pic-02/pic-ma02/uploads/de3238dbbe2fad54ae77f7a011307ca5/20200324153937.png)

```
munao:~ mum$ cd/Users/mum/Documents/Marsbook 
-bash: cd/Users/mum/Documents/Marsbook: No such file or directory
```

重新使用cd 关联文件
```
munao:Marsbook mum$ git push origin master
Everything up-to-date
munao:Marsbook mum$ git add --all
munao:Marsbook mum$ git commit -m"1"
[master 4ee88a5] 1
 1 file changed, 1 insertion(+), 1 deletion(-)
munao:Marsbook mum$ git push origin master
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 4 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 278 bytes | 278.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To gitlab.com:nex-fab/fab-01/marsma.git
```

成功

关于Markdown的常见问题与知识参考此网页

[Markdown](https://sspai.com/post/25137)

* 3.VScode使用

目录：把文件夹拖到VScode中
![](https://gitlab.com/pic-02/pic-ma02/uploads/0f196557097a2c1425b8f4e1b9302c3f/20200403093139.png)

编写目录：
```
# Summary


## Tutorials
* [1.Project manage]()
    * [Assessment](doc/1projectmanage/Assessment1project-manage.md)
    * [Tool](doc/1projectmanage/Tool1Project-manage.md)
    * [How to use Git official document](https://git-scm.com/docs/gittutorial)
    * [PRACTIVE](1/1.1/1.1.md)

* [2.CAD design]()
    * [PRACTIVE](1/1.2/1.2.md)
* [3. 3D printer]()
    * [PRACTIVE](1/1.3/1.3.md)
* [4. Electric design ]()
    * [PRACTIVE](1/1.4/1.4.md)
```
内容与左侧目录对应
![](https://gitlab.com/pic-02/pic-ma02/uploads/2ebee570dffd4927f2f4acbe6f42e6c0/20200403094143.png)

目录完成，上传文件

分别输入

```
git add --all
git commit -m"1"
git push origin master
```
上传完成
