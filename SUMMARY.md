# Summary


## Tutorials
* [1.Project manage]()
    * [Assessment](doc/1projectmanage/Assessment1project-manage.md)
    * [Tool](doc/1projectmanage/Tool1Project-manage.md)
    * [How to use Git official document](https://git-scm.com/docs/gittutorial)
    * [PRACTIVE](1/1.1/1.1.md)

* [2.CAD design]()
    * [PRACTIVE](1/1.2/1.2.md)
    * [Download & Install](1/1.2/sw18.md)
    * [SolidWorks-practive](1/1.2/sw-practice.md)
* [3. 3D printer]()
    * [PRACTIVE](1/1.3/1.3.md)
* [4. Electric design ]()
    * [PRACTIVE](1/1.4/1.4.md)
* [6. Laser cutter ]()
    * [PRACTIVE](1/1.6/1.6.md)
